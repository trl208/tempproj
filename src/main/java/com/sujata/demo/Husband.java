package com.sujata.demo;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

public class Husband {
	@Id
	private int hId;
	private String hName;
	@OneToOne
	@JoinColumn(name = "wife_id")
	private Wife wife;
	public Husband(int hId, String hName, Wife wife) {
		super();
		this.hId = hId;
		this.hName = hName;
		this.wife = wife;
	}
	public int gethId() {
		return hId;
	}
	public void sethId(int hId) {
		this.hId = hId;
	}
	public String gethName() {
		return hName;
	}
	public void sethName(String hName) {
		this.hName = hName;
	}
	public Wife getWife() {
		return wife;
	}
	public void setWife(Wife wife) {
		this.wife = wife;
	}
	
	

}
