package com.sujata.demo;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

public class Wife {
	
	@OneToOne  @Id
	private int wId;
	private String wName;
	@OneToOne
	@JoinColumn(name = "HusbandId")
	private Husband husband;
	public Wife(int wId, String wName, Husband husband) {
		super();
		this.wId = wId;
		this.wName = wName;
		this.husband = husband;
	}
	public int getwId() {
		return wId;
	}
	public void setwId(int wId) {
		this.wId = wId;
	}
	public String getwName() {
		return wName;
	}
	public void setwName(String wName) {
		this.wName = wName;
	}
	public Husband getHusband() {
		return husband;
	}
	public void setHusband(Husband husband) {
		this.husband = husband;
	}

}
