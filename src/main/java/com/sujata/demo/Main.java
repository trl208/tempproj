package com.sujata.demo;

class Example {
    static int x;
    void inc( ) {
        x++;
    }
}
public class Main {
    public static void main(String args[ ]) {
        Example obj1 = new Example( );
        Example obj2 = new Example( );
        obj1.x = 0;
        obj1.inc( );
        obj2.inc( );
        System.out.println(obj1.x + " " + obj2.x);
    }
}